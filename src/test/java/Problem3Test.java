import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem3Test {

    @Test
    void decodeMessage() {

        /* this line was BAD COPING!! */
      //assertEquals(Problem3.decodeMessage(9, 6, "4JKJ'P[OL'JZ'VYWV'MJ5VU[V'UY[O'HVHYLR[P[V'YHK'S[HTLKHY"),
      
        /* this line it's ok, acournding to PDF!! */
        assertEquals(Problem3.decodeMessage(9, 6, "4JKJ'PZ'[OL'[YHKLTHYR'VM'[OL'JVU[YVS'KH[H'JVYWVYH[PVU5"),
                "-CDC IS THE TRADEMARK OF THE CONTROL DATA CORPORATION.");
        
        /* this line was BAD COPING too!! */
        //assertEquals(Problem3.decodeMessage(10, 7, "1PIT'PZ{LYUH{'UHJOPPHPT{PUV'''HVLU{LZYU'HYOZV5JSH{LWYV'K'UPZ|ILMV'RYHT"),
        //        "*IBM IS A tRADEMARK OF tHE INtERNAtIONAL BuSINESS MACHINE CORPORAtION.");
        
        //"1KLJ'pz'{ol'{yhklthyr'vm'{ol'Kpnp{hs'Lx|pwtlu{'Jvywvyh{pvu5"
        assertEquals(Problem3.decodeMessage(10, 7, "1PIT'pz'{ol'{yhklthyr'vm'{ol'Kpnp{hs'Lx|pwtlu{'Jvywvyh{pvu5"),
                "*IBM IS THE TRADEMARK OF THE DIGITAL EQUIPMENT CORPORATION.");
    }
}
