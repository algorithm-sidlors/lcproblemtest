import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
 Problem1: Count pairs with given sum
 Given an array of integers, and a number 'sum', 
 find the number of pairs of integers in the array 
 whose sum is equal to 'sum'.

 Expected time complexity O(n)
*/

public class Problem1Test {

    @Test
    public void testGetPairsCount() {

        /*
         * Input  :  arr[] = {1, 5, 7, -1},
         * sum = 6
         * Output :  2
         */
        int[] arr = {1, 5, 7, -1};
        assertEquals(Problem1.getPairsCount(arr, 6), 2);

        /*
         * Input  :  arr[] = {1, 5, 7, -1, 5},
         * sum = 6
         * Output :  3
         */
        int[] arr2 = {1, 5, 7, -1, 5};
        assertEquals(Problem1.getPairsCount(arr2, 6), 3);

        /*
         * Input  :  arr[] = {1, 1, 1, 1},
         * sum = 2
         * Output :  3
         */
        int[] arr3 = {1, 1, 1, 1};
        assertEquals(Problem1.getPairsCount(arr3, 2), 6);

        /*
         * Input  :  arr[] = {10, 12, 10, 15, -1, 7, 6, 5, 4, 2, 1, 1, 1},
         * sum = 11
         * Output :  9
         */
        int[] arr4 = {10, 12, 10, 15, -1, 7, 6, 5, 4, 2, 1, 1, 1};
        assertEquals(Problem1.getPairsCount(arr4, 11), 9);
    }
 

	@Test
	public void testBestComplexity(){
		//getPairsCountLinearComplexity(

		/*
         * Input  :  arr[] = {1, 5, 7, -1},
         * sum = 6
         * Output :  2
         */
        int[] arr = {1, 5, 7, -1};
        assertEquals(Problem1.getPairsCountLinearComplexity(arr, 6), 2);
        System.out.println("-------------------");
        /*
         * Input  :  arr[] = {1, 5, 7, -1, 5},
         * sum = 6
         * Output :  3
         */
        int[] arr2 = {1, 5, 7, -1, 5};
        assertEquals(Problem1.getPairsCountLinearComplexity(arr2, 6), 3);
        System.out.println("-------------------");

        /*
         * Input  :  arr[] = {1, 1, 1, 1},
         * sum = 2
         * Output :  3
         */
        int[] arr3 = {1, 1, 1, 1};
        assertEquals(Problem1.getPairsCountLinearComplexity(arr3, 2), 3);
        System.out.println("-------------------");

        /*
         * Input  :  arr[] = {10, 12, 10, 15, -1, 7, 6, 5, 4, 2, 1, 1, 1},
         * sum = 11
         * Output :  9
         */
        int[] arr4 = {10, 12, 10, 15, -1, 7, 6, 5, 4, 2, 1, 1, 1};
        assertEquals(Problem1.getPairsCountLinearComplexity(arr4, 11), 9);


	}
}
