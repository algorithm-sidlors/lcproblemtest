import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
Globant is an on-line shop that sells computer parts. Pairs
of in-line electrical connectors are among the most popular parts that Globant sells. However, they are
also one of the parts that are returned more often by unsatisfied customers, because due to errors in
packaging the connectors sent to the costumers may not be compatible.

An in-line connector is composed of five connection points, labelled from 1 to 5. Each connection
point of a connector can be either a plug or an outlet. We say two connectors are compatible if, for
every label, one connection point is a plug and the other connection point is an outlet (in other words,
two connectors are compatible if, for every connection point with the same label, a plug and an outlet
meet when the two connectors are connected).

Globant is introducing a state-of-the-art Outlet Checking Machine, with an optical
checker, which will verify whether the two connectors packaged for a customer are indeed compati-
ble. The complex and expensive hardware of the ACM is ready, but they need your help to finish the
software.

Given the descriptions of a pair of in-line connectors, your task is to determine if the connectors are
compatible.

Input
The input contains several test cases; each test case is formatted as follows. The first line contains five
integers Xi (0 <=Xi <= 1 for i = 1, 2 ... 5), representing the connection points of the first connector
in the pair. The second line contains five integers Yi (0 <= Yi <= 1 for i = 1, 2, ... 5), representing the
connection points of the second connector. In the input, a `0' represents an outlet an a `1' represents
a plug.

Output
For each test case in the input, output a line with a character representing whether the connectors are
compatible or not. Return True or False
 */

class Problem2Test {

    @Test
    void isOutletCompatible() {

        assertEquals(Problem2.isOutletCompatible("1 1 0 1 0", "0 0 1 0 1"), true);
        assertEquals(Problem2.isOutletCompatible("1 0 0 1 0", "1 0 1 1 0"), false);
        assertEquals(Problem2.isOutletCompatible("1 1 1 1 1", "0 0 1 0 0"), false);
        assertEquals(Problem2.isOutletCompatible("1 1 0 0 0", "0 0 1 1 1"), true);
        assertEquals(Problem2.isOutletCompatible("1 0 0 0 0", "1 0 1 1 1"), false);
    }
}
