
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class Problem1 {

    //Expected time complexity O(n)
    public static int getPairsCount(int[] arr, int sum) {
        int count = 0; // Initialize result
        int n = arr.length;
        // Consider all possible pairs and check their sums
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (arr[i] + arr[j] == sum)
                    count++;

        return count;
    }

    
	/*
	 it is a map with posible candidates that distance with 'sum' is the 
	the complement of 'sum'

	Order Complexity is O(n)=n
	*/
	public static int getPairsCountLinearComplexity(int[] arr,  int sum) {

		Map<Integer, Integer> map = new HashMap<Integer, Integer>(); 

		int count=0;

        for (int i = 0; i < arr.length; ++i) { 
            int temp = sum - arr[i]; 
            
            if (map.containsKey(temp)) { 

                //we hit a pair
                System.out.println("Here a Pair with given sum " + sum + 
						" -> (" + arr[i] + ", " + temp + ")"); 
                count=count+map.get(temp);
                continue;
            } 
            //we count repet values in case of a hit add to counter
            if(map.containsKey(arr[i])){
                map.put(arr[i],map.get(arr[i])+1);
            }else{
                map.put(arr[i],1);
            }
        }

        return count;
    }

}
