public class Problem2 {

    //transfor string to boolean variables. O(n)=n+m=n
    //using 'xor' operator and 'not' operator
    public static boolean isOutletCompatible(String outlet1, String outlet2) {

    	boolean[] outletBoolean1=stringToBoolean(outlet1);
    	boolean[] outletBoolean2=stringToBoolean(outlet2);

        for(int i=0;i<outletBoolean1.length;i++){
            if(!(outletBoolean1[i] ^ outletBoolean2[i])){
                return false;
            }
        }

        return true;
    }

    private static boolean[] stringToBoolean(String s){

        s=s.replace(" ","");
    	boolean[] booleanVariables = new boolean[s.length()];
    	char[] c=s.toCharArray();

    	for(int i=0;i<c.length;i++){
    		booleanVariables[i]=c[i]!='0';
    	}
    	return booleanVariables;
    }
}
