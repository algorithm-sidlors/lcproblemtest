# LCJavaProblemSet

This is a set of problems that any Java Developer in the LendingClub account should be able to solve.

Problem1: **Count pairs with given sum**

Given an array of integers, and a number ‘sum’, find the number of pairs of integers in the array whose sum is equal to ‘sum’.

REVIEW AND MAKE ANY CHANGES TO OPTIMIZE THE SOLUTION, Expected time complexity O(n).
___
Problem2: **Outlet Checking Machine**

Globant is introducing a state-of-the-art Outlet Checking Machine, with an optical
checker, which will verify whether the two connectors packaged for a customer are indeed compatible.

The complex and expensive hardware of the OCM is ready, but they need your help to finish the software.
SOLVE THE PROBLEM
___
Problem3: Espionage Decoder

Please look at the Problem3.pdf file for instructions
SOLVE THE PROBLEM

WE LOOK FOR THE OPTIMAL SOLUTION NOT ANY SOLUTION, THINK AND TAKE YOUR TIME

Create a PR with your solution!!! Make sure ALL Test are running OK.

GOOD LUCK. :)

